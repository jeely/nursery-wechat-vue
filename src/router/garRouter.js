/**
 * @教育局端路由配置
 * */
import GarIndex from 'components/garden/garIndex'
import Position from 'components/garden/bus/position'
import QueryBus from 'components/garden/bus/queryBus'
import EduCore from 'components/garden/core/core'
import EduInform from 'components/garden/inform/inform'
import MsgDetail from 'components/garden/inform/msgDetail'
import MyInform from 'components/garden/inform/myInform'
import SendMsg from 'components/garden/inform/sendMsg'
import Speed from 'components/garden/inform/speed'
import NurDis from 'components/garden/nurDis/nurDis'
import NurStatic from 'components/garden/nurStatic/static'
import QueryNur from 'components/garden/queryNur/queryNur'
import NurDetail from 'components/garden/queryNur/nurDetail'

export default [{
		path: '/garIndex',
		name: 'garIndex',
		component: GarIndex
	}, {
		path: '/position',
		name: 'position',
		component: Position
	}, {
		path: '/queryBus',
		name: 'queryBus',
		component: QueryBus
	}, {
		path: '/core',
		name: 'core',
		component: EduCore
	}, {
		path: '/inform',
		name: 'inform',
		component: EduInform
	}, {
		path: '/msgDetail',
		name: 'msgDetail',
		component: MsgDetail
	}, {
		path: '/myInform',
		name: 'myInform',
		component: MyInform
	}, {
		path: '/sendMsg',
		name: 'sendMsg',
		component: SendMsg
	}, {
		path: '/speed',
		name: 'speed',
		component: Speed
	}, {
		path: '/nurDis',
		name: 'nurDis',
		component: NurDis
	}, {
		path: '/static',
		name: 'static',
		component: NurStatic
	}, {
		path: '/queryNur',
		name: 'queryNur',
		component: QueryNur
	}, {
		path: '/nurDetail',
		name: 'nurDetail',
		component: NurDetail
	}

]