/*
 *@教师、家长、园长端路由配置
 * */
import Vue from 'vue'
import Router from 'vue-router'
import GarRouter from './garRouter.js'

import MainIndex from 'components/mainIndex'
import ChooseRole from 'components/chooseRole'
import Binding from 'components/binding'
import Indexs from 'components/main/index'
import School from 'components/school/school'
import Circles from 'components/circle/circles'
import SendMem from 'components/circle/sendMem'
import CloudTeach from 'components/cloudTeach/cloudTeach'
import CloudDetails from 'components/cloudTeach/cloudDetails'
import CloudList from 'components/cloudTeach/cloudList'
import PersonCollection from 'components/myCore/personCollection'
import CircleDetail from 'components/circle/circleDetail'
import MyCore from 'components/myCore/myCore'
import InformManage from 'components/inform/LeadSchool/InformManage'
import InformIssue from 'components/inform/LeadSchool/InformIssue'
import InformProgress from 'components/inform/LeadSchool/InformProgress'
import InformInfo from 'components/inform/LeadSchool/InformInfo'
import tInformManage from 'components/inform/teacher/tInformManage'
import tInformIssue from 'components/inform/teacher/tInformIssue'
import tInformProgress from 'components/inform/teacher/tInformProgress'
import tInformInfo from 'components/inform/teacher/tInformInfo'
import pInformManage from 'components/inform/patriarch/pInformManage'
import pInformInfo from 'components/inform/patriarch/pInformInfo'
import Contacts from 'components/contacts/Contacts'
import ContactsList from 'components/contacts/ContactsList'
import BabyOnLine from 'components/babyOnLine/BabyOnLine'
import Recipe from 'components/recipe/Recipe'
import Stat from 'components/Stat/Stat'
import ChildStar from 'components/childStar/childStar'
import EditChildStar from 'components/childStar/editChildStar'
import AddChildStar from 'components/childStar/addChildStar'
import contactsPatriarch from 'components/contacts/contactsPatriarch'
import contactsTeacher from 'components/contacts/contactsTeacher'
import babyOnLineParent from 'components/babyOnLine/babyOnLineParent'
import SignUp from 'components/signUp'
import AllSignUp from 'components/signUpInfo/allSignUp'
import SignUpDetail from 'components/signUpInfo/signUpDetail'
import SchoolBus from 'components/schoolBus/schoolBus'
import BusLocation from 'components/schoolBus/busLocation'
import ParkInfo from 'components/parkInfo'
import Sitting from 'components/sitting/sitting'
import Relatives from 'components/sitting/relatives'
import Auth from 'components/main/auth'
import Druglist from 'components/drugs/durglist'	// 用药提醒
import SendDrug from 'components/drugs/sendDrugs'
import LeaveList from 'components/leave/leaveList'	//在线请假
import AddLeave from 'components/leave/addLeave'
import LeaveDetail from 'components/leave/leaveDetail'
import Relay from 'components/relay/relay'	//接送记录
import RelayDetail from 'components/relay/relayDetail'

Vue.use(Router)

let rout = [{
		path: "/",
		redirect: '/mainIndex'
	},
	{
		path: '/mainIndex',
		name: 'mainIndex',
		component: MainIndex,
		redirect: '/index',
		children: [{
				path: '/index',
				name: 'index',
				component: Indexs
			}, {
				path: '/school',
				name: 'school',
				component: School
			}, {
				path: '/circles',
				name: 'circles',
				component: Circles
			}, {
				path: '/circleDetail',
				name: 'circleDetail',
				component: CircleDetail
			}, {
				path: '/sendMem',
				name: 'sendMem',
				component: SendMem
			}, {
				path: '/cloudTeach',
				name: 'cloudTeach',
				component: CloudTeach
			}, {
				path: '/cloudDetails',
				name: 'cloudDetails',
				component: CloudDetails
			}, {
				path: '/cloudList',
				name: 'cloudList',
				component: CloudList
			}, {
				path: '/myCore',
				name: 'myCore',
				component: MyCore
			}, {
				path: '/informManage',
				name: 'informManage',
				component: InformManage
			}, {
				path: '/informIssue',
				name: 'informIssue',
				component: InformIssue
			}, {
				path: '/informProgress',
				name: 'informProgress',
				component: InformProgress
			}, {
				path: '/informInfo',
				name: 'informInfo',
				component: InformInfo
			}, {
				path: '/contacts',
				name: 'contacts',
				component: Contacts
			}, {
				path: '/contactsList',
				name: 'contactsList',
				component: ContactsList
			}, {
				path: '/contactsPatriarch',
				name: 'contactsPatriarch',
				component: contactsPatriarch
			}, {
				path: '/contactsTeacher',
				name: 'contactsTeacher',
				component: contactsTeacher
			}, {
				path: '/babyOnLine',
				name: 'babyOnLine',
				component: BabyOnLine
			}, {
				path: '/babyOnLineParent',
				name: 'babyOnLineParent',
				component: babyOnLineParent
			}, {
				path: '/recipe',
				name: 'recipe',
				component: Recipe
			}, {
				path: '/childStar',
				name: 'childStar',
				component: ChildStar
			}, {
				path: '/editChildStar',
				name: 'editChildStar',
				component: EditChildStar
			}, {
				path: '/addChildStar',
				name: 'addChildStar',
				component: AddChildStar
			}, {
				path: '/stat',
				name: 'stat',
				component: Stat
			},
			{
				path: '/parkInfo',
				name: 'parkInfo',
				component: ParkInfo
			},
			{
				path: '/schoolBus',
				name: 'schoolBus',
				component: SchoolBus
			}, {
				path: '/busLocation',
				name: 'busLocation',
				component: BusLocation
			}, {
				path: '/allSignUp',
				name: 'allSignUp',
				component: AllSignUp
			}, {
				path: '/signUpDetail',
				name: 'signUpDetail',
				component: SignUpDetail
			}, {
				path: '/personCollection',
				name: 'personCollection',
				component: PersonCollection
			}, {
				path: '/sitting',
				name: 'sitting',
				component: Sitting
			}, {
				path: '/relatives',
				name: 'relatives',
				component: Relatives
			}, {
				path: '/tinformManage',
				name: 'tinformManage',
				component: tInformManage
			}, {
				path: '/tinformIssue',
				name: 'tinformIssue',
				component: tInformIssue
			}, {
				path: '/tinformProgress',
				name: 'tinformProgress',
				component: tInformProgress
			}, {
				path: '/tinformInfo',
				name: 'tinformInfo',
				component: tInformInfo
			}, {
				path: '/pInformManage',
				name: 'pInformManage',
				component: pInformManage
			}, {
				path: '/pInformInfo',
				name: 'pInformInfo',
				component: pInformInfo
			}, {				//11111
				path: '/druglist',
				name: 'druglist',
				component: Druglist
			}, {
				path: '/sendDrug',
				name: 'sendDrug',
				component: SendDrug
			}, {
				path: '/leaveList',
				name: 'leaveList',
				component: LeaveList
			}, {
				path: '/addLeave',
				name: 'addLeave',
				component: AddLeave
			}, {
				path: '/leaveDetail',
				name: 'leaveDetail',
				component: LeaveDetail
			}, {
				path: '/relay',
				name: 'relay',
				component: Relay
			}, {
				path: '/relayDetail',
				name: 'relayDetail',
				component: RelayDetail
			}
		]
	}, {
		path: '/chooseRole',
		name: 'chooseRole',
		component: ChooseRole
	}, {
		path: '/binding',
		name: 'binding',
		component: Binding
	},
	{
		path: '/signUp',
		name: 'signUp',
		component: SignUp
	},{
		path: '/auth',
		name: 'auth',
		component: Auth
	}
]

rout = rout.concat(GarRouter)

export default new Router({
	routes: rout

})