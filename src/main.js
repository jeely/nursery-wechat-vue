// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import api from 'api/index'
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.config.productionTip = false
import Vuex from 'vuex'
import uploader from 'vue-easy-uploader'
import http from 'axios'
import VueLazyLoad from 'vue-lazyload'
Vue.use(VueLazyLoad,{
    error:'./static/loading.png',
    loading:'./static/loading.png'
})
Vue.use(Mint)
/*定义全局api方法*/
Vue.use(Vuex)
let store = new Vuex.Store({})
Vue.use(uploader, store)
Vue.prototype.$api = api
Vue.prototype.$http = http
let Base64 = require('js-base64').Base64;
/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
    store,
	components: {
		App
	},
	template: '<App/>'
})
