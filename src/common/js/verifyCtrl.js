/**
 * Created by Administrator on 2017/11/15.
 */
export function isChinaName(name) {
    var pattern = /^[\u4E00-\u9FA5]{2,6}$/;
    return pattern.test(name);
}

// 验证手机号
export function isPhoneNo(phone) {
    var pattern = /^1[1-9]\d{9}$/;
    return pattern.test(phone);
}
export function CheckPhone(number){
    console.log(number)
    var reg = /^((0\d{2,3}-\d{7,8})|(1[3584]\d{9}))$/;
    return reg.test(number);
}

export function isTitle(parameter) {
    if (parameter.length<1||parameter.length>20){
        return false;
    }
    return true;
}
// 验证邮箱
export function isMail(mail) {
    return /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(mail)
}

//验证快递单号
/*EMS ^[C,E][A-Z][0-9]{9}(CN)$
申通E物流 ^(268|888|588|688|998)[0-9]{9}$
圆通速递 ^(0|1|2|3|E|D|F)[0-9]{9}$
中通速递 ^((618|680|828|571|518)[0-9]{9})$|^(2008[0-9]{8})$|^((00|10)[0-9]{10})$
宅急送 ^[0-9]{10}$
韵达快运 ^[0-9]{13}$
天天快递 ^[0-9]{14}$
风火天地 ^[0-9]{10}$
华强物流 ^[A-Za-z0-9]*[0|2|4|6|8]$
联邦快递 ^[0-9]{12}$*/
export function isCourierNumber(num) {
    var pattern = /^[0-9a-zA-Z]{10,30}$/g;
    return pattern.test(num);
}
//验证正整数
export function testNumber(num){
    console.log(num)
    var pattern = /^[0-9]\d{0,99999999}$/;
    return pattern.test(num);
    console.log(num)
}
export function testNumbers(num){
    console.log(num)
    var pattern = /^[1-9]\d{0,9999}$/;
    return pattern.test(num);
}
export function testLen(num){
    console.log(num)
    var numBer = parseFloat(num);
    if(numBer<10000000&&numBer>=0){
        return true;
        console.log(numBer)
    }else{
        return false
    }
}

// 验证座机号
export function isPhoneNum(phone) {
    var pattern = /^0\d{10,12}$/;
    return pattern.test(phone);
}
// 验证座机号
//export function isPhoneNum(phone) {
//  var pattern = /^(0\d{2,3}-?)?\d{7,8}$/;
//  return pattern.test(phone);
//}

// 验证身份证
export function isCardNo(card) {
    var pattern = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return pattern.test(card);
}
//验证银行卡号
export function isBank(num) {
    var pattern = /\d{15}|\d{19}/;
    if (pattern.test(num)||num=="") {
        return true;
    }else{
        return false;
    };
}
//随机数
export function randoms() {
        var num= Math.random()*100000
        return parseInt(num);
    }
//身份证号合法性验证
//支持15位和18位身份证号
//支持地址编码、出生日期、校验位验证
export function IdentityCodeValid(code) {
    var pass= true;

    if(!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)){
        pass = false;
    }
    else{
        //18位身份证需要验证最后一位校验位
        // if(code.length == 18){
        //     code = code.split('');
        //     //∑(ai×Wi)(mod 11)
        //     //加权因子
        //     var factor = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ];
        //     //校验位
        //     var parity = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ];
        //     var sum = 0;
        //     var ai = 0;
        //     var wi = 0;
        //     for (var i = 0; i < 17; i++)
        //     {
        //         ai = code[i];
        //         wi = factor[i];
        //         sum += ai * wi;
        //     }
        //     var last = parity[sum % 11];
        //     if(parity[sum % 11] != code[17]){
        //         pass =false;
        //     }
        // }
    }
    return pass;
}
/**
 * 验证密码6-20数字和字母组合
 * @param pw
 * @returns {boolean}
 */
export function isPassword(pw) {

    var patrn1=/[\u4e00-\u9fa5]+/;
    if(!patrn1.test(pw)){
        var patrn=/^(\S){5,16}$/;
        return patrn.test(pw);
    }else{
        return false;
    }
    }

export function isPhone(pw) {
    var patrn=/^(\d){7,11}$/;
    return patrn.test(pw);
}
export function isNameLenght(pw) {
    var patrn=/^[\da-z\u2E80-\u9FFF]{1,15}$/i;
    return patrn.test(pw);
}
export function isPasd(pd) {
    var patrn=/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
    return patrn.test(pd);
}
//校验登录名：只能输入5-20个以字母开头、可带数字、“_”、“.”的字串
export function isRegisterUserName(s)
{
    var patrn=/^[a-zA-Z0-9_-]{5,16}$/;
    if (!patrn.exec(s)) return false
    return true
}
//价格验证
export function isPrice(s)
{
    var patrn=/^(0|[1-9][0-9]{0,9})(\.[0-9]{1,2}){0,7}$/;
    if (!patrn.exec(s)) return false
    return true
}
/**
 *验证非空
 */
export function isEmpty(parameter) {
    if(parameter==""||parameter==undefined||parameter==null)
        return false;
    if (parameter.length<1||parameter.length>10000){
        return false;
    }
    return true;
}
export function  isedictor(parameter) {
    if (parameter.length<1){
        return false;
    }
    return true;
}
export function isMessage(parameter) {
    if (parameter.length<1||parameter.length>200){
        return false;
    }
    return true;
}
export function isAddress(address) {
    if (address.length<1||address.length>50){
        return false;
    }
    return true;
}

/**
 * 判断中文字符长度
 * @param parameter 参数
 * @param startLen 长度
 * @returns {boolean}
 */
export function strLen(parameter,startLen,endLen) {
    if(!parameter){
        return false;
    }else{
        var abc=parameter.replace(/[^\x00-\xff]/g, '**');
        if (abc.length>endLen||abc.length<startLen)
            return false;
    }
    return true;
}
/**
 * 验证是否是金钱格式
 * @param money
 * @returns {boolean}
 */
export function isMoney(money) {
    var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
    if (reg.test(money)||money=="") {
        return true;
    }else{
        return false;
    };
}


/**
 * psl
 * 验证字符长度以及输入限制
 *parameter 验证字符
 *startLen  最小长度
 *endLen  最大长度
 * type 1验证字母，数字  2验证字母，数字，中文 3验证字母，中文 4验证数字
 * */
export function checkChar(parameter,startLen,endLen,type) {
    var len = 0;
    var falg=true;
    if(parameter==""||parameter==undefined||parameter==null)
        falg= false;
    for (var i = 0; i < parameter.length; i++) {
        var a = parameter.charAt(i);
        if (a.match(/[^\x00-\xff]/ig) != null) {
            len += 2;
        } else {
            len += 1;
        }
    }

    /**判断是否达到最小长度,超出最大长度*/
    if (len<startLen||len>endLen)
        return false;
    switch(type)
    {
        case 1:
           if(!isW(parameter)){
               falg= false;
           }
           break;
        case 2:
            if(!isWCH(parameter)){
                falg= false;
            }
            break;
        case 3:
            if(!isACH(parameter))
                falg= false;
            break;
        case 4:
            if(!isD(parameter))
                falg= false;
            break;
        default:
            break;
    }
    return falg;
}
/**
 * psl
 * 验证字母，数字
 * */
export function isW(parameter) {
    var patrn=/^[A-Za-z0-9]+$/g;
    return patrn.test(parameter);
}
/**
 * psl
 * 验证字母，数字，中文
 * */
export function isWCH(parameter) {
    var patrn=/^[\w\u4e00-\u9fa5]+$/g;
    return patrn.test(parameter);
}
/**
 * psl
 * 验证字母，中文
 * */
export function isACH(parameter) {
    var patrn=/^[\u4e00-\u9fa5A-Za-z]+$/g;
    return patrn.test(parameter);
}
/**
 * psl
 * 验证数字
 * */
export function isD(parameter) {
    var patrn=/^\d+$/g;
    return patrn.test(parameter);
}