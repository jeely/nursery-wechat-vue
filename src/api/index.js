import axios from 'axios'
import qs from 'qs'
axios.interceptors.request.use(config => { // 这里的config包含每次请求的内容
	// 判断localStorage中是否存在api_token
	if(localStorage.getItem('api_token')) {
		//  存在将api_token写入 request header
		config.headers.apiToken = `${localStorage.getItem('api_token')}`;
	}
	return config;
}, err => {
	return Promise.reject(err);
});

axios.interceptors.response.use(response => {
	return response
}, error => {
	return Promise.resolve(error.response)
});

//var baseUrl = "http://192.168.1.41:9001/";
//var baseUrls = "http://192.168.1.41:9005/"
var baseUrl = "http://192.168.1.200:8086/";
var baseUrls = "http://192.168.1.200:8089/"
function checkStatus(response) {
	// 如果http状态码正常，则直接返回数据
	if(response && (response.status === 200 || response.status === 304 ||
			response.status === 400)) {
		return response.data
	}
	// 异常状态下，把错误信息返回去
	return {
		status: -404,
		msg: '网络异常'
	}
}

function checkCode(res) {
	// 如果code异常(这里已经包括网络错误，服务器错误，后端抛出的错误)，可以弹出一个错误提示，告诉用户
	if(res.status === -404) {
		alert(res.msg)
	}
	if(res.data && (!res.data.success)) {
		// alert(res.data.error_msg)
	}
	return res
}
// 请求方式的配置
export default {
	post(url, data) { //  post
		return axios({
			method: 'post',
			baseURL: baseUrl,
			//baseURL: baseUrl,
			 url: url+'?'+Math.random()*100000,
			data: qs.stringify(data),
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	get(url, params) { // get
		return axios({
			method: 'get',
			baseURL: baseUrl,
			 url: url+'?'+Math.random()*100000,
			params, // get 请求时带的参数
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	put(url, params) { // get
		return axios({
			method: 'put',
			baseURL: baseUrl,
			 url: url+'?'+Math.random()*100000,
			params, // get 请求时带的参数
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	delete(url, params) { // get
		return axios({
			method: 'delete',
			baseURL: baseUrl,
			 url: url+'?'+Math.random()*100000,
			params, // get 请求时带的参数
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	//多文件上传
	uploadFile(url, file, name, data) {
		var formData = new FormData();
		var baseURL = 'http://192.168.1.41:9002/'
		for(var i in data) {
			formData.append('file', data[i]);
		}
		console.log(formData);
		return axios({
			method: 'POST',
			baseURL: baseURL,
			 url: url+'?'+Math.random()*100000,
			data: formData,
			timeout: 5000,
			headers: {
				'Content-Type': 'multipart/form-data'

			}

		}).then(
			(response) => {
				console.log(response)
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	//单文件上传
	uploadFiles(url, file, name, data) {
		var formData = new FormData();
		formData.append(name, file);
		for(var i in data) {
			formData.append(i, data[i]);
		}
		console.log(formData);
		return axios({
			method: 'POST',
			baseURL: baseUrl,
			 url: url+'?'+Math.random()*100000,
			data: formData,
			timeout: 5000,
		}).then(
			(response) => {
				console.log(response)
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	/**
	 *@圈子模块请求方法
	 */
	posts(url, data) { //  post
		return axios({
			method: 'post',
			baseURL: baseUrls,
			//baseURL: baseUrl,
			 url: url+'?'+Math.random()*100000,
			data: qs.stringify(data),
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	gets(url, params) { // get
		return axios({
			method: 'get',
			baseURL: baseUrls,
			 url: url+'?'+Math.random()*100000,
			params, // get 请求时带的参数
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	puts(url, params) { // get
		return axios({
			method: 'put',
			baseURL: baseUrls,
			 url: url+'?'+Math.random()*100000,
			params, // get 请求时带的参数
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	},
	deletes(url, params) { // get
		return axios({
			method: 'delete',
			baseURL: baseUrls,
			 url: url+'?'+Math.random()*100000,
			params, // get 请求时带的参数
			timeout: 5000,
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		}).then(
			(response) => {
				return checkStatus(response)
			}
		).then(
			(res) => {
				return checkCode(res)
			}
		)
	}

}
